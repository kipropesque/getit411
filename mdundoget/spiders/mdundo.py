# -*- coding: utf-8 -*-
import scrapy
from scrapy.http.request import Request
from mdundoget.items import MdundoArtistItem, Mdundo404Item, MdundoSongItem
from urlparse import urlparse
from mdundoget.database.dbconnect import DBconnect

class MdundoSpider(scrapy.Spider):
    name = 'mdundo'
    allowed_domains = ['mdundo.com']
    
    def __init__(self):
        self.uri = "http://mdundo.com/genre"

    def start_requests(self):
        countries = ['ke', 'ug', 'tz']
        for genre in [1, 2, 4, 8, 32, 64]:
            for country in countries:
                url = self.uri + ("/%s/%s" % (genre, country))
                yield Request(url, self.parse_artists_page)

    def parse_artists_page(self, response):
        artist_nodes = response.xpath('//div[@class="artist_wrapper"]/ul/li/div[@class="artist"]')

        for artist_node in artist_nodes:
            artist_url = artist_node.xpath(
                '//div[@class="artist_thumbnail"]/a[@class="thumbnail_action thumbnail_action_empty"]/@href'
            ).extract()[0]

            yield Request(artist_url, self.parse_artist)

    def parse_artist(self, response):
        artist_name = response.xpath('//div[@class="single-artist_content_top"]/h1[@class="single-artist_name"]/text()').extract()
        artist_rank = response.xpath('//div[@class="single-artist_subinfo"]/div[@class="single-artist_rank"]/text()').extract()
        
        song_node_list = response.xpath('//ul[@class="song_list"]/li')
        artist_id = urlparse(response.request.url).path.split('/').pop()

        for song_node in song_node_list:
            song_low_quality_url = song_node.xpath('//a[@class="btn btn-download"]').extract()[0]
            song_title = song_node.xpath('//a[@class="song-info"]/span[@class="song-title"]/text()').extract()[0]
            song_genre = song_node.xpath('//div[@class="song-genre"]/a/text()').extract()[0]
            song_genre_url = song_node.xpath('//div[@class="song-genre"]/a/@href').extract()[0]

            song_item = MdundoSongItem()
            item['song_name'] = song_heading_node.extract()
            item['song_genre'] = song_genre
            item['artist_id'] = artist_id
            item['asset_url'] = song_low_quality_url

            try:
                item['song_id'] = urlparse(song_low_quality_url).path.split('/').pop()
            except IndexError as e:
                item['song_id'] = None

            yield song_item
            
            yield Request(song_genre_url, self.parse_genre)

        artist_item = MdundoArtistItem()
        artist_item['artist_name'] = artist_name
        artist_item['artist_id'] = artist_id
        if artist_rank:
            artist_item['artist_rank'] = int(artist_rank[0].split(':')[1])
        yield artist_item

        related_artists = response.xpath(
            '//section[@class="separate_section discover"]/div/div[@class="section_content"]/div[@class="artist_wrapper artist_wrapper__in-row"]/ul/li'
        )
        for related_artist in related_artists:
            related_artist_url = related_artist.xpath('//div[@class="artist"]/div[@class="artist_thumbnail"]/a/@href').extract()[0]
            yield Request(related_artist_url, self.parse_artist)

    def parse_genre(self, response):
        song_node_list = response.xpath('//div[@class="song-list_wrapper"]/ul/li')

        for song_node in song_node_list:
            song_url = song_node.xpath('//a[@class="song-info"]/span[@class="song-title"]/@href').extract()

            yield Request(song_url, self.parse_song)

    def parse_song(self, response):
        song_heading_node = response.xpath('//div[@class="single-song_heading"]/h1/text()')

        if song_heading_node:
            artist_url = response.xpath('//div[@class="single-song_heading"]/h2/a/@href').extract()
            yield Request(artist_url, self.parse_artist)
        else:
            yield Mdundo404Item()



