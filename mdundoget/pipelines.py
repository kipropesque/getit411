# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from mdundoget.database.dbconnect import DBconnect
from MySQLdb import Error
from scrapy.utils.project import get_project_settings
from mdundoget.items import MdundoArtistItem, Mdundo404Item, MdundoSongItem

class MdundogetPipeline(object):
	def __init__(self):
		dbconn = DBconnect()
		self.db = dbconn.connect()
		self.cursor = self.db.cursor()
		settings = get_project_settings()

	def process_item(self, item, spider):
		print "start pipeline to db"
			
		if type(item) == MdundoSongItem:
			try:
				#Insert songs; check if we already have them
				self.cursor.execute(
					"""
					SELECT * FROM songs where md_id = %s AND genre is null
					""" % item.get('song_id').encode()
				)

				if self.cursor.rowcount == 0:
					if item.get('song_id') is not None and item.get('song_name') is not None and item.get('artist_id') is not None:
						self.cursor.execute(
							"""
							INSERT INTO songs (name, md_id, artist_id, file_url, genre) VALUES (%s, %s, %s, %s)
							""", (
								item.get('song_name')[0].encode('utf-8'), 
								item.get('song_id'),
								item.get('artist_id'),
								item.get('asset_url')[0].encode('utf-8'),
								item.get('song_genre'),
							)
						)
				else:
					self.cursor.execute(
						"""
						UPDATE songs 
						SET genre = %s
						WHERE md_id = %s
						""", (
							item.get('song_genre'),
							item.get('song_id')
						)
					)

				self.db.commit()
			except Error as e:
				print "Error %d: %s" % (e.args[0], e.args[1])

		elif type(item) == MdundoArtistItem:
			try:
				self.cursor.execute(
					"""
					SELECT * FROM artists where md_id = %s
					""" % item.get('artist_id').encode()
				)

				if self.cursor.rowcount == 0:
					self.cursor.execute(
						"""
						INSERT INTO artists (name, md_id, rank) VALUES (%s, %s, %s)
						""", (
							item.get('artist_name')[0].encode('utf-8'), 
							item.get('artist_id'),
							item.get('artist_rank')
						)
					)
				else:
					self.cursor.execute(
						"""
						UPDATE artists 
						SET rank = %s
						WHERE md_id = %s
						""", (
							item.get('artist_rank'),
							item.get('artist_id')
						)
					)

				self.db.commit()
			except Error as e:
				print "Error %d: %s" % (e.args[0], e.args[1])

		elif type(item) == Mdundo404Item:
			print "item is 404", item