#!/usr/bin/python

#Returns mysql cursor

import MySQLdb
from scrapy.utils.project import get_project_settings

class DBconnect():
	def __init__(self, name=None, host=None, user=None, passwd=None):
		db_config = get_project_settings().get("DATABASE", {})

		self.name = db_config.get('name', name)
		self.host = db_config.get('host', host)
		self.user = db_config.get('user', user)
		self.passwd = db_config.get('password', passwd)
	
	def connect(self):
		db = MySQLdb.connect(
			self.host, 
			self.user, 
			self.passwd, 
			self.name, 
			charset="utf8", 
			use_unicode=True
		)
		return db

	@staticmethod
	def get_all_undownloaded_songs():
		settings = get_project_settings()
		all_songs = range(
			settings.get('MDUNDO_SONG_START_INDEX'), 
			settings.get('MDUNDO_SONG_STOP_INDEX')
		)

		db = DBconnect()
		cursor = db.connect().cursor()

		cursor.execute(
			"""
			SELECT md_id FROM songs;
			"""
		)
		
		downloaded_songs = [id[0] for id in cursor.fetchall()]
		return sorted(list(set(all_songs) - set(downloaded_songs)))