CREATE TABLE if not exists artists (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(250) NOT NULL,
	md_id INT NOT NULL
) ENGINE=InnoDB;

create table if not exists songs (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(250) NOT NULL,
	md_id INT NOT NULL,
	artist_id INT NOT NULL,
	filename VARCHAR(250),
	file_url VARCHAR(250)
) ENGINE=InnoDB