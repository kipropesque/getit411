# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html
import scrapy

class MdundoItem(scrapy.Item):
    def __unicode__(self):
        print "<MdundoItem>"

class Mdundo404Item(MdundoItem):
    def __unicode__(self):
        print "<Mdundo404Item>"

class MdundoSongItem(MdundoItem):
    song_name = scrapy.Field()
    song_id = scrapy.Field()
    artist_id = scrapy.Field()
    song_genre = scrapy.Field()
    asset_url = scrapy.Field()

    def __unicode__(self):
        print (
            ("<MdundoSongItem ") +
            ("song_name=%s" % self.song_name) +
            ("song_id=%s" % self.song_id) +
            ("asset_url=%s" % self.asset_url)
        )

class MdundoArtistItem(MdundoItem):
    artist_name = scrapy.Field()
    artist_id = scrapy.Field()
    artist_rank = scrapy.Field()

    def __unicode__(self):
        print (
            ("<MdundoArtistItem ") +
            ("artist_name=%s" % self.artist_name) +
            ("artist_id=%s" % self.artist_id)
        )
